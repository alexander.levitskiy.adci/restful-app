<?php

namespace Drupal\restful_app;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;

/**
 * Implements class RestfulContentService.
 *
 * @package Drupal\restful_app
 */
class RestfulContentService implements RestfulContentServiceInterface {

  use StringTranslationTrait;

  /**
   * The node storage object.
   */
  protected $nodeStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The node query object.
   */
  protected $nodeQuery;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Creates an RestfulContentService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, DateFormatterInterface $date_formatter) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->currentUser = $current_user;
    $this->nodeQuery = $entity_type_manager->getStorage('node')->getQuery();
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllContent($range = 1) {
    $data = [];

    // Get all articles.
    $this->nodeQuery->condition('type', 'article');
    $this->nodeQuery->condition('status', 1);
    $this->nodeQuery->sort('created', 'DESC');
    $this->nodeQuery->range(($range - 1) * 10, 10);
    $nids = $this->nodeQuery->execute();

    if ($nids) {
      $nodes = $this->nodeStorage->loadMultiple($nids);
      $data['code'] = 200;
      foreach ($nodes as $key => $node) {
        $data['content'][] = [
          'uuid' => $node->uuid(),
          'title' => $node->getTitle(),
          'body' => $node->get('body')->getValue(),
          'created' => $this->dateFormatter->format($node->getCreatedTime(), 'c'),
        ];
      }
    }
    else {
      $data = ['code' => 204, 'message' => $this->t('No Content')];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getNode(NodeInterface $node) {
    return [
      'code' => 200,
      'content' => [
        'uuid' => $node->uuid(),
        'title' => $node->getTitle(),
        'body' => $node->get('body')->getValue(),
        'created' => $this->dateFormatter->format($node->getCreatedTime(), 'c'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createContent(array $content, $bundle = 'article') {
    $node = $this->nodeStorage->create([
      'type'        => $bundle,
    ]);

    // Preparing the node content before saving.
    $this->preparingContent($content);

    // Set the node values.
    foreach ($content as $field_name => $field_value) {
      $node->set($field_name, $field_value);
    }

    try {
      $node->save();
    }
    catch (EntityStorageException $e) {
      watchdog_exception('restful_app', $e);
      return [
        'code' => 400,
        'message' => $e->getMessage(),
      ];
    }

    return [
      'code' => 201,
      'message' => $this->t('The node created successful.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function updateContent(NodeInterface $node, array $content) {

    // Set values for the node.
    foreach ($content as $field_name => $field_value) {
      $node->set($field_name, $field_value);
    }
    try {
      $node->save();
    }
    catch (EntityStorageException $e) {
      watchdog_exception('restful_app', $e);
      return [
        'code' => 400,
        'message' => $e->getMessage(),
      ];
    }

    return [
      'code' => 200,
      'message' => $this->t('The node @title updated successful.', [
        '@title' => $node->getTitle(),
      ]
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteContent(NodeInterface $node) {
    $title = $node->getTitle();
    try {
      $node->delete();
    }
    catch (EntityStorageException $e) {
      watchdog_exception('restful_app', $e);
      return [
        'code' => 400,
        'message' => $e->getMessage(),
      ];
    }

    return [
      'code' => 200,
      'message' => $this->t('The node @title deleted successful.', [
        '@title' => $title,
      ]
      ),
    ];
  }

  /**
   * The helper functions for preparing node values.
   *
   * @param array $content
   *   The node values.
   */
  private function preparingContent(array &$content) {
    foreach ($content as $key => $item) {
      switch ($key) {
        case 'created':
          $date = new DrupalDateTime($content['created']);
          $content[$key] = $date->getTimeStamp();

          break;

        case 'body':
          $content[$key] = [
            'value' => $content[$key],
            'format' => 'basic_html',
          ];
          break;
      }
    }
  }

}
