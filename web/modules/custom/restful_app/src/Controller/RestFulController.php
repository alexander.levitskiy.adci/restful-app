<?php

namespace Drupal\restful_app\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\restful_app\RestfulContentServiceInterface;
use Drupal\user\UserInterface;
use Laminas\Diactoros\Response\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implements class RestFulController.
 *
 * @package Drupal\restful_app\Controller
 */
class RestFulController extends ControllerBase {

  /**
   * Drupal \Drupal\restful_app\RestfulContentService definition.
   *
   * @var \Drupal\restful_app\RestfulContentService
   */
  protected $restfulContentService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The node storage object.
   */
  protected $nodeStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * RestFulController constructor.
   *
   * @param \Drupal\restful_app\RestfulContentServiceInterface $restful_content_service
   *   The RestfulContentService object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(RestfulContentServiceInterface $restful_content_service, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->restfulContentService = $restful_content_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('restful_app.restful_content_service'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Get all articles handler.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Laminas\Diactoros\Response\JsonResponse
   *   Return the json response with result.
   */
  public function getAllArticles(Request $request) {
    $page = $request->get('page');
    $response = $page ? $this->restfulContentService->getAllContent($page) : $this->restfulContentService->getAllContent();

    return new JsonResponse($response);
  }

  /**
   * Get article by uuid.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   *
   * @return \Laminas\Diactoros\Response\JsonResponse
   *   Return the json response with result.
   */
  public function getArticle(RouteMatchInterface $route_match) {
    $node = $this->nodeStorage->loadByProperties([
      'uuid' => $route_match->getParameter('articleUuid'),
    ]);
    if (!empty($node)) {
      $node = current($node);
      if ($node instanceof NodeInterface) {
        $response = $this->restfulContentService->getNode($node);

        return new JsonResponse($response);
      }
    }

    return new JsonResponse(['code' => 204, 'message' => $this->t('No Content')]);
  }

  /**
   * Create an article handler.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Laminas\Diactoros\Response\JsonResponse
   *   Return the json response with result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createArticle(Request $request) {
    $content = $request->request->all();

    return new JsonResponse($this->restfulContentService->createContent($content));
  }

  /**
   * Update an article handler.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   *
   * @return \Laminas\Diactoros\Response\JsonResponse
   *   Return the json response with result.
   */
  public function updateArticle(Request $request, RouteMatchInterface $route_match) {
    $content = $request->request->all();
    if (!isset($content['uuid'])) {
      return new JsonResponse([$this->t('For updating the node you have to provide node uuid.')]);
    }

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load($content['uuid']);
    if (!$node) {
      return new JsonResponse([
        'code' => 204,
        'message' => $this->t('The node with uuid: @uuid is not found.', [
          '@uuid' => $content['uuid'],
        ]),
      ]);
    }

    $node = $this->nodeStorage->loadByProperties([
      'uuid' => $route_match->getParameter('articleUuid'),
    ]);

    if (!empty($node)) {
      $node = current($node);
      if ($node instanceof NodeInterface) {
        $response = $this->restfulContentService->updateContent($node, $content);

        return new JsonResponse($response);
      }
    }

    return new JsonResponse([
      'code' => 406,
      'message' => $this->t('Something get wrong, please contact site administrator.'),
    ]);
  }

  /**
   * Delete an article handler.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   *
   * @return \Laminas\Diactoros\Response\JsonResponse
   *   Return the json response with result.
   */
  public function deleteArticle(RouteMatchInterface $route_match) {
    $node = $this->nodeStorage->loadByProperties([
      'uuid' => $route_match->getParameter('articleUuid'),
    ]);

    if (!empty($node)) {
      $node = current($node);
      if ($node instanceof NodeInterface) {
        $response = $this->restfulContentService->deleteContent($node);

        return new JsonResponse($response);
      }
    }

    return new JsonResponse([
      'code' => 204,
      'message' => $this->t('No found article with this uuid: @uuid', [
        '@uuid' => $route_match->getParameter('articleUuid'),
      ]),
    ]);
  }

  /**
   * Custom access callback for creating the article.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Return access results.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createArticleAccess() {
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());

    if (!($user instanceof UserInterface)) {
      return AccessResult::forbidden($this->t('You have not access to this action.'));
    }
    elseif (!$user->hasRole('administrator')) {
      return AccessResult::forbidden($this->t('You have not access to this action.'));
    }

    return AccessResult::allowed();
  }

  /**
   * Custom access callback for deleting article.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Return access results.
   */
  public function deleteArticleAccess() {
    $request = \Drupal::request();

    if (substr($request->getClientIp(), 0, 3) === '198' && $request->getPort() === 443) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden($this->t('You have not access to this action.'));
  }

}
