<?php

namespace Drupal\restful_app;

use Drupal\node\NodeInterface;

/**
 * Implements the RestfulContentServiceInterface.
 *
 * @package Drupal\restful_app
 */
interface RestfulContentServiceInterface {

  /**
   * The method with getting all content based on range.
   *
   * @return array
   *   Return array of the data.
   */
  public function getAllContent($range = 0);

  /**
   * Get a single node data.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array
   *   Return array of the node data.
   */
  public function getNode(NodeInterface $node);

  /**
   * Creating the node.
   *
   * @param array $content
   *   The node values.
   * @param string $bundle
   *   The node bundle.
   *
   * @return array
   *   Return array of the data.
   */
  public function createContent(array $content, $bundle = 'article');

  /**
   * Updating the node by uuid.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   * @param array $content
   *   The node values.
   *
   * @return array
   *   Return array of the data.
   */
  public function updateContent(NodeInterface $node, array $content);

  /**
   * Delete the node by uuid.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array
   *   Return array of the data.
   */
  public function deleteContent(NodeInterface $node);

}
